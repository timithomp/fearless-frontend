// Create a new JavaScript file to handle loading the available locations and submitting the conference data.
// The value of the options for the locations select tag should be the value of the "id" property of the location object.
//  (This is comparable to what you did on the last page with setting the value of the option to the "abbreviation" property.)
// Notice that the JSON expects a numeric identifier for the location.
// You'll need to change the location list API to include the id property of locations.
// Make sure you POST to the correct API URL.
// Clear the data from the form when it succeeds.



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const locationTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option')
            const select = document.querySelector('select')
            const locationId = location["id"]
            option.value = locationId
            const locationName = location["name"]
            option.innerHTML = locationName
            select.appendChild(option)
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            console.log('need to submit the form data');
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json, "-----this is json------")
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            console.log(fetchConfig, "this is fetch config")
            const response = await fetch(conferenceUrl, fetchConfig);
            console.log(response, "------this is response")
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                    console.log(newConference);
                }
        });
    };
});

window.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('login-form');
    // const navbar = document.getElementsByClassName('nav-link')
    form.addEventListener('submit', async event => {
      event.preventDefault();
      const data = Object.fromEntries(new FormData(form))
      const fetchOptions = {
        method: 'post',
        body: new FormData(form),
        credentials: "include",
      };
      const url = 'http://localhost:8000/login/';
      const response = await fetch(url, fetchOptions);
      if (response.ok) {

        // for (let nav in navbar){
        //   console.log(navbar, "this is navbar starting for loop")
        // ------------- returns an HTMLObject :'( --------------------------
        //   console.log(navbar[nav], "-----before removenb[nav]")
        // ------------- returns correct <a> but "d-none" missing from class list :'( --------------
        //   navbar[nav].classList.remove('d-none')
        //   console.log(navbar[nav], "-----nb[nav] after .remove-----")
        // }
// ------------------ this does not work at all lol ---------------------------------------
        // console.log("we have now exited the for loop")
        window.location.href = '/';

      } else {
        console.error(response);
      }
    });
  });

function createCard(name, description, pictureUrl, dateStart, dateEnd, location) {
  return `
    <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${dateStart} - ${dateEnd}
      </div>
    </div>
  `;
};
function createAlert(errorMessage){
  return `<div class="alert alert-warning" role="alert">
  There was an error in the request: ${errorMessage}
</div>`;
};
// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         console.log(conference, "----- data.conference[0] ------")
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             console.log(details, "---- details -----")

//             const conferenceDescription = details.conference.description;
//             const decriptonTag = document.querySelector('.card-text');
//             decriptonTag.innerHTML = conferenceDescription;

//             const imageTag = document.querySelector('.card-img-top')
//             const pictureUrl = details.conference.location.picture_url;
//             console.log(pictureUrl, "---- picture url -----")

//             imageTag.src = pictureUrl;
//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }
//   });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    const errorDiv = document.querySelector('.errormsghandler')
    let colIndex = 0


    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000x ${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details, "---details")
            const name = details.conference.name;
            console.log(name, "-----name")
            const description = details.conference.description;
            console.log(description, "-----description")
            const pictureUrl = details.conference.location.picture_url;
            console.log(pictureUrl)
            const startDateRaw = new Date(details.conference.starts)
            console.log(startDateRaw, "--------start")
            const startDateYear = startDateRaw.getFullYear()
            const startDateMonth = startDateRaw.getMonth() + 1
            const startDateDay = startDateRaw.getDate()
            const startDateFormatted = `${startDateDay}/${startDateMonth}/${startDateYear}`
            console.log(startDateFormatted, "-----start formatted")
            const endDateRaw = new Date(details.conference.ends)
            console.log(endDateRaw, "----end raw")
            const endDateYear = endDateRaw.getFullYear()
            const endDateMonth = endDateRaw.getMonth() + 1
            const endDateDay = endDateRaw.getDate()
            const endDateFormatted = `${endDateDay}/${endDateMonth}/${endDateYear}`
            console.log(endDateFormatted, "----formatted end")
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startDateFormatted, endDateFormatted, location);
            console.log(html, "----html")
            const column = columns[colIndex % 3]
            column.innerHTML += html;
            colIndex++;

          }
        }
      }
    } catch (e) {
      const column = columns[colIndex % 3]
      column.innerHTML = createAlert(e)
    }
  });
